# OpenML dataset: SensIT-Vehicle-Combined

https://www.openml.org/d/1593

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: University of Wisconsin–Madison  
libSVM","AAD group  
**Source**: [original](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/multiclass.html) - Date unknown  
**Please cite**:   

#Dataset from the LIBSVM data repository.

Preprocessing: Regenerate features by the authors' matlab scripts (see Sec. C of Appendix A), then randomly select 10% instances from the noise class so that the class proportion is 1:1:2 (AAV:DW:noise). The training/testing sets are from a random 80% and 20% split of the data.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1593) of an [OpenML dataset](https://www.openml.org/d/1593). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1593/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1593/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1593/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

